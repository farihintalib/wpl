<?php

namespace App\Http\Middleware;

use App\Services\Wordpress\WordpressService;
use Closure;
use Illuminate\Http\Request;

class WpSession {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        (new WordpressService())->wpSession();
        return $next($request);
    }

}
