<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\Wordpress\WordpressService;

class Permission {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $ability) {
        if (!(new WordpressService())->hasAny($ability)) {
            abort(403, 'Unauthorized action.');
        }
        return $next($request);
    }

}
