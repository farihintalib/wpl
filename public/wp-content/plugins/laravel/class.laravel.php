<?php

use App\Http\Middleware\EncryptCookies;
use App\Services\Wordpress\WordpressService;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Pipeline;
use Illuminate\Session\Middleware\StartSession;

class Laravel {

    public function init() {
        require base_path("vendor/autoload.php");
        $app = require_once base_path("bootstrap/app.php");
        if (!is_bool($app)) {
            $kernel = $app->make(Kernel::class);
            $request = Request::capture();
            $app->instance('request', $request);
            $kernel->bootstrap();
            $response = (new Pipeline($app))
                    ->send($request)
                    ->through([
                        EncryptCookies::class,
                        AddQueuedCookiesToResponse::class,
                        StartSession::class
                    ])
                    ->then(function () {
                return response('', 200, []);
            });
            foreach ($response->headers->getCookies() as $cookie) {
                if ($cookie->isRaw()) {
                    setrawcookie($cookie->getName(), $cookie->getValue(), $cookie->getExpiresTime(), $cookie->getPath(), $cookie->getDomain(), $cookie->isSecure(), $cookie->isHttpOnly());
                } else {
                    setcookie($cookie->getName(), $cookie->getValue(), $cookie->getExpiresTime(), $cookie->getPath(), $cookie->getDomain(), $cookie->isSecure(), $cookie->isHttpOnly());
                }
            }
        }
    }

    public function login($userLogin) {
        (new static)->init();
        $wordpressService = new WordpressService();
        $wordpressService->login($userLogin);
    }

    public function logout($userLogin) {
        (new static)->init();
        $wordpressService = new WordpressService();
        $wordpressService->logout($userLogin);
    }

    public function redirect404() {
        if (is_404()) {
            echo view('errors.404');
            exit();
        }
    }

}
