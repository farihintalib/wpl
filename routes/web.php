<?php

use App\Http\Controllers\Scaffold\ScaffoldController;
use App\Services\Wordpress\WordpressService;
use Illuminate\Support\Facades\Route;
use Mft89\Starter\Controllers\StarterController;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/laravel', function () {
    $wordpressService = new WordpressService();
    $avatar = $wordpressService->getAvatarUrlCurrentUser();
    $wpVersion = $wordpressService->getWpVersion();
    return view('laravel.index', compact('avatar', 'wpVersion'));
});

Route::group(['middleware' => ['auth', 'permission:alertManagementManage']], function () {
    Route::get('/laravel-auth-permission', function () {
        $wordpressService = new WordpressService();
        $avatar = $wordpressService->getAvatarUrlCurrentUser();
        $wpVersion = $wordpressService->getWpVersion();
        return view('laravel.index', compact('avatar', 'wpVersion'));
    });
});

Route::prefix('scaffolds')->group(function () {
    Route::get('/', [ScaffoldController::class, 'index'])->name('scaffolds.index');
    Route::get('install-step1', [StarterController::class, 'installStep1'])->name('starter.install-step1');
    Route::get('install-step2', [StarterController::class, 'installStep2'])->name('starter.install-step2');
    Route::get('update-starter', [StarterController::class, 'updateStarter'])->name('starter.update-starter');
    Route::post('/{table}', [StarterController::class, 'scaffold'])->name('starter.scaffold');
    Route::post('update-to-starter', [StarterController::class, 'updateToStarter'])->name('starter.update-to-starter');
});
