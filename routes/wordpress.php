<?php

use App\Http\Controllers\Wordpress\WordpressController;
use Illuminate\Support\Facades\Route;

Route::get('/', [WordpressController::class, 'index'])->name('wordpress.index');
Route::any('/{any}', [WordpressController::class, 'index'])->name('wordpress.any')->where('any', '(.*)');
