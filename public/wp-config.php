<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', config('database.connections.mysql.database'));

/** MySQL database username */
define('DB_USER', config('database.connections.mysql.username'));

/** MySQL database password */
define('DB_PASSWORD', config('database.connections.mysql.password'));

/** MySQL hostname */
define('DB_HOST', config('database.connections.mysql.host'));

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', config('database.connections.mysql.charset'));

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', config('database.connections.mysql.collation'));

if (!defined('WP_CLI')) {
    define('WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']);
    define('WP_HOME', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']);
}



/* * #@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'r7Jtkhuc9mwCz63MJwEJjFxECAuEILxmr33vDh760PbVSgQHPX5CUIYmX2lddewb');
define('SECURE_AUTH_KEY', 'hYbOfki5Mr0uvOXNXcgkfaSf1lFSxX3ssRr1P4bpGIJsIZQPe9rbFL6BshouRPL0');
define('LOGGED_IN_KEY', 'dMJ7BCDQSuo66a7J95PdWj83x0cjehbvvCWyyAFe9fbqtxrtrF1XZiezQ1uPAtNT');
define('NONCE_KEY', '9AhoYUZCYxDHFxfFiXp7DsnO6WKKUBkyzmr6i904BMCoe738EcAKjPnxdIAbk8PD');
define('AUTH_SALT', 'exx3kryrlRPh71zJIRJP4YASsrb6RVNeGyXc1mBu66Fd5t2nxZoUcaYhPBLtumMw');
define('SECURE_AUTH_SALT', 'z9xfS9RX9raCHdKLK3uwEuuMa1S4rSCI7B7rS7ASzgTCfY1zWzoSnp5QpWU70pQr');
define('LOGGED_IN_SALT', 'IKt32X7cC60oG7w55KVZmLRyRLSxiCrQ3L3Bndpx4iYAVSK5I4vY6u2ivHlYH7zz');
define('NONCE_SALT', '0PusLBWlzDFwAYpZr9lNoTx35LUeebgvHhNorMZiyyLGmFmDVbsmZOcVOklXpjMn');

/* * #@- */

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = config('database.connections.mysql.prefix');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', __DIR__ . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
