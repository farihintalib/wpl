@extends('layout.laravel.app')

@section('main-content')
    @component('layout.laravel.component.frame',['wpVersion'=>$wpVersion])
        @slot('content')
            <div class="p-6" style="width: 100vw;max-width: 1000px;">
                <div class="card card-primary card-tabs">
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="scaffoldTab">
                                @if(session()->has('success'))
                                    <div class="alert alert-primary">
                                        {!! Session::get('success') !!}
                                    </div>
                                @endif
                                <form>
                                    @include('scaffold.form')
                                </form>
                                @if($scaffolds)
                                    <div class="row">
                                        <div class="col-3">
                                            <div class="list-group" id="list-tab" role="tablist">
                                                @foreach($scaffolds as $scaffold)
                                                    <a class="list-group-item list-group-item-action {{$scaffold['active']}}" data-cm="cm{{$scaffold['name']}}" data-toggle="list" href="#list-{{$scaffold['name']}}" role="tab">{{$scaffold['title']}}</a>
                                                    @push('scriptsDocumentReady')
                                                        var cm{{$scaffold['name']}} = initCodeMirror(document.getElementById("{{$scaffold['name']}}"));
                                                    @endpush
                                                @endforeach
                                                @if($table && config('crefuna.scaffold'))
                                                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#list-scaffold" role="tab">Scaffolds</a>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-9">
                                            <div class="tab-content" id="nav-tabContent">
                                                @foreach($scaffolds as $scaffold)
                                                    <div class="tab-pane fade {{$scaffold['active']}}" id="list-{{$scaffold['name']}}" role="tabpanel">
                                                        <textarea id="{{$scaffold['name']}}" style="width: -webkit-fill-available;height: 90vh;font-size: 13px;">{{$scaffold['stub']}}</textarea>
                                                    </div>
                                                @endforeach
                                                @if($table && config('crefuna.scaffold'))
                                                    <div class="tab-pane fade" id="list-scaffold" role="tabpanel">
                                                        <form action="{{route('starter.scaffold',[$table])}}" method="post">
                                                            @csrf
                                                            @foreach($scaffolds as $scaffold)
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input" name="{{$scaffold['name']}}" value="yes">
                                                                    <label class="form-check-label" for="{{$scaffold['name']}}">{{$scaffold['title']}}</label>
                                                                </div>
                                                            @endforeach
                                                            <input type="hidden" class="form-control" id="generate_path" name="generate_path" value="{{$generatePath}}">
                                                            <input type="hidden" class="form-control" id="module" name="module" value="{{$module}}">
                                                            <button type="submit" class="btn btn-primary">Generate</button>
                                                        </form>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endslot
    @endcomponent
@endsection