<?php

namespace App\Services\Option;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class OptionService {

    public function select($method) {
        if (method_exists($this, $method)) {
            return array_map(function($ins) {
                return $ins['data-value'];
            }, $this->{$method}());
        } else {
            return [];
        }
    }

    // generate id at https://www.uuidgenerator.net/version1
    public function themes(array $input = []) {
        return [
            'fbc4' => [
                'data-selected' => $this->selected($input['selected'] ?? '', 'fbc4'),
                'data-color' => '',
                'data-value' => __('AdminLTE 2'),
                'data-key' => 'fbc4',
            ],
            '11eb' => [
                'data-selected' => $this->selected($input['selected'] ?? '', '11eb'),
                'data-color' => '',
                'data-value' => __('AdminLTE 3'),
                'data-key' => '11eb',
            ],
            'a8b3' => [
                'data-selected' => $this->selected($input['selected'] ?? '', 'a8b3'),
                'data-color' => '',
                'data-value' => __('Metronic'),
                'data-key' => 'a8b3',
            ],
        ];
    }
    
    public function database(array $input = []){
        $listDatabase = [];
        foreach(DB::select('SHOW DATABASES') as $database){
            $listDatabase[$database->Database] = [
                'data-selected' => $this->selected($input['selected'] ?? '', $database->Database),
                'data-color' => '',
                'data-value' => $database->Database,
                'data-key' => $database->Database,
            ];
        }
        return $listDatabase;
    }

    public function scaffoldName(array $input = []) {
        return [
            'basic' => [
                'data-selected' => $this->selected($input['selected'] ?? '', 'basic'),
                'data-color' => '',
                'data-value' => __('Basic'),
                'data-key' => 'basic',
            ],
            'advance' => [
                'data-selected' => $this->selected($input['selected'] ?? '', 'advance'),
                'data-color' => '',
                'data-value' => __('Advance'),
                'data-key' => 'advance',
            ],
        ];
    }

    public function yesNo(array $input = []) {
        return [
            'yes' => [
                'data-selected' => $this->selected($input['selected'] ?? '', 'yes'),
                'data-color' => 'success',
                'data-value' => __('Yes'),
                'data-key' => 'yes',
            ],
            'no' => [
                'data-selected' => $this->selected($input['selected'] ?? '', 'no'),
                'data-color' => 'danger',
                'data-value' => __('No'),
                'data-key' => 'no',
            ],
        ];
    }

    public function selected($select, $key) {
        return $select == $key ? 'selected' : '';
    }

}
