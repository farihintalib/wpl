<?php

namespace App\Services\Wordpress;

use App\Models\User;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Http\Request;

class WordpressService {

    protected $userModel;

    public function __construct() {
        require base_path("public/wp-load.php");
        $this->userModel = new User();
    }

    public function getAvatarUrlCurrentUser() {
        return get_avatar_url(wp_get_current_user()->ID);
    }

    public function getWpVersion() {
        global $wp_version;
        return substr($wp_version, 0, 3);
    }

    public function hasAny($ability) {
        if (auth()->check()) {
            $abilities = is_array($ability) ? $ability : explode('|', $ability);
            foreach (wp_get_current_user()->allcaps as $key => $value) {
                foreach ($abilities as $valueAbility) {
                    if ($key == $valueAbility && $value == true) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function login($userLogin) {
        $user = $this->userModel->where('user_login', $userLogin)->first();
        auth()->loginUsingId($user->ID);
        $request = Request::capture();
        return app()->make(Kernel::class)->handle($request);
    }

    public function wpSession() {
        if (wp_get_current_user()->ID == 0) {
            $this->logout();
        }
        return;
    }

    public function logout() {
        auth()->logout();
        request()->session()->invalidate();
        request()->session()->regenerateToken();
        return;
    }

}
