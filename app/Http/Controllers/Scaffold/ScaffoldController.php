<?php

namespace App\Http\Controllers\Scaffold;

use App\Http\Controllers\Controller;
use App\Services\Wordpress\WordpressService;
use App\Traits\Scaffold\ScaffoldTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class ScaffoldController extends Controller {

    use ScaffoldTrait;

    protected $stubPath;
    protected $generatePath;
    protected $module;
    protected $database;

    public function __construct() {
        if (request()->generate_path) {
            $this->generatePath = request()->generate_path;
        } else {
            $this->generatePath = base_path('temp');
        }
        if (request()->module) {
            if (Str::startsWith(request()->module, "\\")) {
                $this->module = request()->module;
            } else {
                $module = request()->module;
                $this->module = "\\{$module}";
            }
        }
        if (request()->theme) {
            $theme = request()->theme;
            $name = request()->name;
            $this->stubPath = "resources\stub\scaffold\\{$theme}\\{$name}";
        }
        if (request()->database) {
            $this->database = request()->database;
        } else {
            $this->database = DB::connection()->getDatabaseName();
        }
    }

    public function index() {
        $wordpressService = new WordpressService();
        $tables = $this->getTableFromDB();
        if (request()->table && File::exists(base_path($this->stubPath)) && $this->getSchemaSelectTableFromDB(request()->table)) {
            if (request()->name == 'basic') {
                $lists = [
                    ['Controller', 'controller', 'show active'],
                    ['Datatable', 'datatable', ''],
                    ['Model', 'model', ''],
                    ['Request Store', 'storerequest', ''],
                    ['Request Update', 'updaterequest', ''],
                    ['Routes', 'routes', ''],
                    ['View Form', 'viewform', ''],
                    ['View Form Script', 'viewformscript', ''],
                    ['View Index', 'viewindex', ''],
                ];
            } elseif (request()->name == 'advance') {
                $lists = [
                    ['Controller', 'controller', 'show active'],
                    ['Datatable', 'datatable', ''],
                    ['Model', 'model', ''],
                    ['Repository', 'repository', ''],
                    ['Scope', 'scope', ''],
                    ['Service', 'service', ''],
                    ['Request Store', 'storerequest', ''],
                    ['Request Update', 'updaterequest', ''],
                    ['Routes', 'routes', ''],
                    ['View Form', 'viewform', ''],
                    ['View Form Script', 'viewformscript', ''],
                    ['View Index', 'viewindex', ''],
                ];
            } else {
                $lists = [];
            }
        }
        if (isset($lists) && $lists) {
            foreach ($lists as $list) {
                $scaffolds[$list[1]] = [
                    'name' => $list[1],
                    'title' => $list[0],
                    'stub' => $this->streamScaffold(request()->table, $list[1]),
                    'active' => $list[2]
                ];
            }
        } else {
            $scaffolds = [];
        }
        $msg = request()->msg ?? null;
        $generatePath = $this->generatePath;
        $module = $this->module;
        $database = $this->database;
        $table = request()->table;
        $fileLists = [];
        $avatar = $wordpressService->getAvatarUrlCurrentUser();
        $wpVersion = $wordpressService->getWpVersion();
        return view('scaffold.index', compact('database', 'wpVersion', 'avatar', 'tables', 'scaffolds', 'generatePath', 'module', 'table', 'msg', 'fileLists'));
    }

}
