@extends('errors.minimal')

@section('title', ___('Not Found'))
@section('code', '404')
@section('message', ___('Not Found'))
