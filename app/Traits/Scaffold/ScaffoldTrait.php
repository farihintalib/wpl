<?php

namespace App\Traits\Scaffold;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Session;

trait ScaffoldTrait {

    protected function getTableFromDB() {
        $databaseConnection = DB::connection()->getName();
        $config = Config::get("database.connections.{$databaseConnection}");
        $modifyConfig = $config;
        $modifyConfig['database'] = $this->database;
        DB::purge($databaseConnection);
        config()->set("database.connections.{$databaseConnection}", $modifyConfig);
        DB::reconnect();
        $tables = DB::connection()->getDoctrineSchemaManager()->listTableNames();
        DB::purge($databaseConnection);
        config()->set("database.connections.{$databaseConnection}", $config);
        DB::reconnect();
        return $tables;
    }

    protected function getSchemaTableFromDB($tableName) {
        $databaseConnection = DB::connection()->getName();
        $config = Config::get("database.connections.{$databaseConnection}");
        $modifyConfig = $config;
        $modifyConfig['database'] = $this->database;
        $modifyConfig['prefix'] = "";
        DB::purge($databaseConnection);
        config()->set("database.connections.{$databaseConnection}", $modifyConfig);
        DB::reconnect();
        $schema = Schema::getColumnListing($tableName);
        DB::purge($databaseConnection);
        config()->set("database.connections.{$databaseConnection}", $config);
        DB::reconnect();
        return $schema;
    }

    protected function getSchemaSelectTableFromDB($tableName) {
        $databaseConnection = DB::connection()->getName();
        $config = Config::get("database.connections.{$databaseConnection}");
        $modifyConfig = $config;
        $modifyConfig['database'] = $this->database;
        config()->set("database.connections.{$databaseConnection}", $modifyConfig);
        DB::reconnect();
        try {
            $schema = DB::select(DB::raw("SHOW COLUMNS FROM {$tableName}"));
        } catch (\Exception $e) {
            $schema = [];

        }
        config()->set("database.connections.{$databaseConnection}", $config);
        DB::reconnect();
        return $schema;
    }

    public function scaffold($tableName) {
        $this->generatePath = request()->generate_path;
        $this->module = request()->module;
        $this->tableName = $tableName;
        $schema = Schema::getColumnListing($tableName);
        $data = [];
        if ($schema != []) {
            array_push($data, $this->handleScaffold($tableName, 'controller'));
            array_push($data, $this->handleScaffold($tableName, 'datatable'));
            array_push($data, $this->handleScaffold($tableName, 'model'));
            array_push($data, $this->handleScaffold($tableName, 'repository'));
            array_push($data, $this->handleScaffold($tableName, 'scope'));
            array_push($data, $this->handleScaffold($tableName, 'service'));
            array_push($data, $this->handleScaffold($tableName, 'storerequest'));
            array_push($data, $this->handleScaffold($tableName, 'updaterequest'));
            array_push($data, $this->handleScaffold($tableName, 'viewform'));
            array_push($data, $this->handleScaffold($tableName, 'viewformscript'));
            array_push($data, $this->handleScaffold($tableName, 'viewindex'));
        }
        $response = [
            'table' => $tableName,
            'generate_path' => $this->generatePath,
            'module' => $this->module
        ];
        return redirect(route('starter.index') . '?' . http_build_query($response))->with('success', implode('<br>', array_filter($data)));
    }

    protected function schemaValidation($definitions, $tableName) {
        $validations = [];
        foreach ($definitions as $definition) {
            $have = false;
            $key = $definition['field'];
            $validation = [];
            if ($definition['null'] == 'NO') {
                $have = true;
                array_push($validation, 'required');
            }
            if ($definition['key'] == 'UNI') {
                $have = true;
                array_push($validation, 'unique:' . $tableName);
            }
            if ($definition['length'] != null) {
                if (in_array($definition['type'], ['int', 'bigint', 'double', 'decimal'])) {
                    $have = true;
                    array_push($validation, 'digits_between:1,' . $definition['length']);
                } else {
                    $have = true;
                    array_push($validation, 'max:' . $definition['length']);
                }
            }
            if (in_array($definition['type'], ['int', 'bigint'])) {
                $have = true;
                array_push($validation, 'integer');
            } elseif (in_array($definition['type'], ['double', 'decimal'])) {
                $have = true;
                array_push($validation, 'numeric');
            }
            if ($have == false) {
                array_push($validation, '');
            }
            $validations[$key] = implode("','", $validation);
        }
        return $this->serviceValidation($validations);
    }

    protected function schemaDetails($tableName) {
        $schema = $this->getSchemaSelectTableFromDB($tableName);
        $definitions = [];
        foreach ($schema as $definition) {
            if (strpos($definition->Extra, 'auto_increment') !== false) {
                $increment = true;
            } else {
                $increment = false;
            }
            $type = explode(' ', trim(str_replace(['(', ')'], ' ', explode(' ', $definition->Type)[0])));
            switch ($type[0]) {
                case 'char':
                case 'varchar':
                case 'int':
                case 'bigint':
                    $typeLength = $type[1];
                    break;
                case 'enum':
                    $typeSet = explode(',', str_replace("'", '', $type[1]));
                    break;
                default :
                    $typeLength = null;
                    $typeSet = null;
            }
            array_push($definitions, [
                'field' => $definition->Field,
                'type' => $type[0],
                'length' => $typeLength ?? null,
                'set' => $typeSet ?? null,
                'null' => $definition->Null,
                'key' => $definition->Key,
                'default' => $definition->Default,
                'increment' => $increment,
            ]);
        }
        return $definitions;
    }

    protected function handleScaffold($tableName, $type) {
        $path = $this->getPath($tableName, $type);
        if (request()->$type == 'yes') {
            $this->makeDirectory($path['directory']);
            File::put($path['path'], $this->buildClass($path['stub'], $tableName, $type));
            return $type . ' created successfully.';
        } else {
            return null;
        }
    }

    protected function streamScaffold($tableName, $type) {
        $path = $this->getPath($tableName, $type);
        return $this->buildClass($path['stub'], $tableName, $type);
    }

    protected function getPath($tableName, $type) {
        $arrayReplacements = $this->arrayReplacements($tableName, $type);
        if ($type == 'model') {
            $directory = $this->generatePath . '\\' . $arrayReplacements['modelRoot'];
            $path = $directory . '\\' . $arrayReplacements['modelName'] . '.php';
            $exist = File::exists($path);
            $stub = '\model.stub';
        } elseif ($type == 'controller') {
            $directory = $this->generatePath . '\\' . $arrayReplacements['controllerRoot'];
            $path = $directory . '\\' . $arrayReplacements['controllerName'] . '.php';
            $exist = File::exists($path);
            $stub = '\controller.model.stub';
        } elseif ($type == 'datatable') {
            $directory = $this->generatePath . '\\' . $arrayReplacements['dataTableRoot'];
            $path = $directory . '\\' . $arrayReplacements['dataTableName'] . '.php';
            $exist = File::exists($path);
            $stub = '\datatables.stub';
        } elseif ($type == 'service') {
            $directory = $this->generatePath . '\\' . $arrayReplacements['serviceRoot'];
            $path = $directory . '\\' . $arrayReplacements['serviceName'] . '.php';
            $exist = File::exists($path);
            $stub = '\service.plain.stub';
        } elseif ($type == 'repository') {
            $directory = $this->generatePath . '\\' . $arrayReplacements['repositoryRoot'];
            $path = $directory . '\\' . $arrayReplacements['repositoryName'] . '.php';
            $exist = File::exists($path);
            $stub = '\repository.plain.stub';
        } elseif (in_array($type, ['storerequest', 'updaterequest'])) {
            $directory = $this->generatePath . '\\' . $arrayReplacements['requestRoot'];
            $path = $directory . '\\' . $arrayReplacements['requestName'] . '.php';
            $exist = File::exists($path);
            $stub = '\request.stub';
        } elseif ($type == 'viewindex') {
            $directory = $this->generatePath . '\\' . $arrayReplacements['viewIndexRoot'];
            $path = $directory . '\index.blade.php';
            $exist = File::exists($path);
            $stub = '\view-index.stub';
        } elseif ($type == 'viewform') {
            $directory = $this->generatePath . '\\' . $arrayReplacements['viewIndexRoot'] . '\form';
            $path = $directory . '\form.blade.php';
            $exist = File::exists($path);
            $stub = '\view-form.stub';
        } elseif ($type == 'viewformscript') {
            $directory = $this->generatePath . '\\' . $arrayReplacements['viewIndexRoot'] . '\form';
            $path = $directory . '\form_script.blade.php';
            $exist = File::exists($path);
            $stub = '\view-form-script.stub';
        } elseif ($type == 'scope') {
            $directory = $this->generatePath . '\\' . $arrayReplacements['scopeRoot'];
            $path = $directory . '\\' . $arrayReplacements['scopeName'] . '.php';
            $exist = File::exists($path);
            $stub = '\scope.stub';
        } elseif ($type == 'repositoryContract') {
            $directory = $this->generatePath . '\\' . $arrayReplacements['repositoryContractRoot'];
            $path = $directory . '\\' . $arrayReplacements['repositoryContractName'] . '.php';
            $exist = File::exists($path);
            $stub = '\contract.repository.stub';
        } elseif ($type == 'routes') {
            $directory = null;
            $path = null;
            $exist = File::exists($path) ?? null;
            $stub = '\routes.stub';
        } elseif ($type == 'serviceContract') {
            $directory = $this->generatePath . '\\' . $arrayReplacements['serviceContractRoot'];
            $path = $directory . '\\' . $arrayReplacements['serviceContractName'] . '.php';
            $exist = File::exists($path);
            $stub = '\contract.service.stub';
        }
        return [
            'directory' => $directory,
            'path' => $path,
            'exist' => $exist,
            'stub' => $stub,
        ];
    }

    protected function makeDirectory($path) {
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
        return $path;
    }

    protected function buildClass($stubPath, $tableName, $type) {
        $stub = File::get(base_path(trim($this->stubPath . $stubPath, '/')));
        $replace = $this->buildReplacements($tableName, $type);
        return str_replace(array_keys($replace), array_values($replace), $stub);
    }

    protected function arrayReplacements($tableName, $type) {
        $schema = $this->getSchemaTableFromDB($tableName);
        $schemaDetails = $this->schemaDetails($tableName);
        $primaryKey = $this->modelPrimaryKey($schemaDetails);
        $modelName = Str::studly(Str::singular($tableName));
        $modelNamespace = 'App\Models' . $this->module;
        $modelRoot = 'app\Models' . $this->module;
        $modelFillable = "'" . implode("','", $this->modelFillable($schemaDetails)) . "'";
        $modelVariable = lcfirst($modelName);
        $modelPrimaryKey = $primaryKey[0] ?? null;
        $modelTimestamps = $this->modelTimestamps($schemaDetails);
        $modelIncrement = $this->modelIncrement($schemaDetails);
        $modelPrimaryOrCompositeKey = "'" . implode("','", $primaryKey) . "'";
        $controllerName = "{$modelName}Controller";
        $controllerNamespace = "App\Http\Controllers{$this->module}";
        $controllerRoot = 'app\Http\Controllers' . $this->module;
        $dataTableName = $modelName . 'Dt';
        $dataTableNamespace = 'App\DataTables' . $this->module . '\\' . $modelName;
        $dataTableRoot = 'app\DataTables' . $this->module . '\\' . $modelName;
        $dataTableVariable = lcfirst($modelName . 'Dt');
        $dataTableEdit = $this->dataTableEdit(array_diff($schema, $primaryKey));
        $dataTableColumn = $this->dataTableColumn(array_diff($schema, $primaryKey));
        $serviceName = $modelName . 'Service';
        $serviceVariable = lcfirst($serviceName);
        $serviceNamespace = 'App\Services' . $this->module;
        $serviceRoot = 'app\Services' . $this->module;
        $serviceValidation = $this->schemaValidation($schemaDetails, $tableName);
        $repositoryName = $modelName . 'Repository';
        $repositoryVariable = lcfirst($repositoryName);
        $repositoryNamespace = 'App\Repositories' . $this->module;
        $repositoryRoot = 'app\Repositories' . $this->module;
        if ($type == 'storerequest') {
            $requestName = 'StoreRequest';
        } elseif ($type == 'updaterequest') {
            $requestName = 'UpdateRequest';
        } else {
            $requestName = 'Request';
        }
        $requestNamespace = 'App\Http\Requests' . $this->module . '\\' . $modelName;
        $requestRoot = 'app\Http\Requests' . $this->module . '\\' . $modelName;

        if (!empty($this->module)) {
            $prefixViewNamespace = '';
            $prefixViewIndexRoot = '';
            foreach (array_filter(explode('\\', $this->module)) as $value) {
                $prefixViewNamespace .= Str::slug($value) . '.';
                $prefixViewIndexRoot .= Str::slug($value) . '\\';
            }
            $viewNamespace = $prefixViewNamespace . Str::slug(Str::singular($tableName));
            $viewIndexRoot = 'resources\views\\' . $prefixViewIndexRoot . Str::slug(Str::singular($tableName));
        } else {
            $viewNamespace = Str::slug(Str::singular($tableName));
            $viewIndexRoot = 'resources\views\\' . $viewNamespace;
        }
        $urlSlug = Str::slug($tableName);
        $viewForm = $this->viewForm(array_diff($schema, $primaryKey));
        $viewFormScript = $this->viewFormScript(array_diff($schema, $primaryKey));
        $scopeName = $modelName . 'Scope';
        $scopeNamespace = 'App\Traits\Scopes' . $this->module;
        $scopeRoot = 'app\Traits\Scopes' . $this->module;
        $serviceContractName = $modelName . 'ServiceContract';
        $serviceContractNamespace = 'App\Contracts\Services' . $this->module;
        $serviceContractRoot = 'app\Contracts\Services' . $this->module;
        $repositoryContractName = $modelName . 'RepositoryContract';
        $repositoryContractNamespace = 'App\Contracts\Repositories' . $this->module;
        $repositoryContractRoot = 'app\Contracts\Repositories' . $this->module;
        return [
            'dateNow' => Carbon::now(),
            'tableName' => $tableName,
            'modelName' => $modelName,
            'modelNamespace' => $modelNamespace,
            'modelRoot' => $modelRoot,
            'modelFillable' => $modelFillable,
            'modelVariable' => $modelVariable,
            'modelPrimaryKey' => $modelPrimaryKey,
            'modelPrimaryOrCompositeKey' => $modelPrimaryOrCompositeKey,
            'modelTimestamps' => $modelTimestamps,
            'modelIncrement' => $modelIncrement,
            'controllerName' => $controllerName,
            'controllerNamespace' => $controllerNamespace,
            'controllerRoot' => $controllerRoot,
            'dataTableName' => $dataTableName,
            'dataTableNamespace' => $dataTableNamespace,
            'dataTableRoot' => $dataTableRoot,
            'dataTableVariable' => $dataTableVariable,
            'dataTableEdit' => $dataTableEdit,
            'dataTableColumn' => $dataTableColumn,
            'serviceName' => $serviceName,
            'serviceVariable' => $serviceVariable,
            'serviceNamespace' => $serviceNamespace,
            'serviceRoot' => $serviceRoot,
            'serviceValidation' => $serviceValidation,
            'repositoryName' => $repositoryName,
            'repositoryVariable' => $repositoryVariable,
            'repositoryNamespace' => $repositoryNamespace,
            'repositoryRoot' => $repositoryRoot,
            'requestName' => $requestName,
            'requestNamespace' => $requestNamespace,
            'requestRoot' => $requestRoot,
            'viewNamespace' => $viewNamespace,
            'viewIndexRoot' => $viewIndexRoot,
            'urlSlug' => $urlSlug,
            'viewForm' => $viewForm,
            'viewFormScript' => $viewFormScript,
            'scopeName' => $scopeName,
            'scopeNamespace' => $scopeNamespace,
            'scopeRoot' => $scopeRoot,
            'serviceContractName' => $serviceContractName,
            'serviceContractNamespace' => $serviceContractNamespace,
            'serviceContractRoot' => $serviceContractRoot,
            'repositoryContractName' => $repositoryContractName,
            'repositoryContractNamespace' => $repositoryContractNamespace,
            'repositoryContractRoot' => $repositoryContractRoot,
        ];
    }

    protected function modelTimestamps($definition, $indentation = 12) {
        $columns = is_array($definition) ? $definition : explode(',', $definition);
        foreach ($columns as $key => $column) {
            if ($column['field'] == 'created_at') {
                return 'true';
            }
        }
        return 'false';
    }

    protected function modelFillable($definition, $indentation = 12) {
        $columns = is_array($definition) ? $definition : explode(',', $definition);
        $arrayColumns = [];
        foreach ($columns as $key => $column) {
            if ($column['key'] != 'PRI') {
                array_push($arrayColumns, $column['field']);
            }
        }
        return $arrayColumns;
    }

    protected function modelIncrement($definition, $indentation = 12) {
        $columns = is_array($definition) ? $definition : explode(',', $definition);
        foreach ($columns as $key => $column) {
            if ($column['increment'] === true) {
                return 'true';
            }
        }
        return 'false';
    }

    protected function modelPrimaryKey($definition, $indentation = 12) {
        $columns = is_array($definition) ? $definition : explode(',', $definition);
        $stub = [];
        foreach ($columns as $key => $column) {
            if ($column['key'] == 'PRI') {
                array_push($stub, $column['field']);
            }
        }
        return $stub;
    }

    protected function viewForm($definition, $indentation = 12) {
        $columns = is_array($definition) ? $definition : explode(',', $definition);
        $stub = '';
        foreach ($columns as $key => $column) {
            $stub .= '<div class="form-group"><label for="' . $column . '">' . $column . '<span class="text-danger">*</span></label><input id="' . $column . '" name="' . $column . '" data-label="' . $column . '" type="text" class="form-control"></div>';
            if ($key < count($columns) - 1) {
                $stub .= PHP_EOL . str_repeat(' ', $indentation);
            }
        }
        return $stub;
    }

    protected function viewFormScript($definition, $indentation = 12) {
        $columns = is_array($definition) ? $definition : explode(',', $definition);
        $stub = '';
        foreach ($columns as $key => $column) {
            $stub .= "$(" . '"' . "form#{{\$id}} [name='" . $column . "']" . '"' . ").val(data." . $column . ").change();";
            if ($key < count($columns) - 1) {
                $stub .= PHP_EOL . str_repeat(' ', $indentation);
            }
        }
        return $stub;
    }

    protected function serviceValidation($definition, $indentation = 12) {
        $columns = is_array($definition) ? $definition : explode(',', $definition);
        $stub = '';
        foreach ($columns as $key => $column) {
            if ($column == '') {
                $stub .= "'{$key}' => [],";
            } else {
                $stub .= "'{$key}' => ['{$column}'],";
            }
            if ($key < count($columns) - 1) {
                $stub .= PHP_EOL . str_repeat(' ', $indentation);
            }
        }
        return $stub;
    }

    protected function dataTableColumn($definition, $indentation = 12) {
        $columns = is_array($definition) ? $definition : explode(',', $definition);
        $stub = '';
        foreach ($columns as $key => $column) {
            $stub .= "Column::make('{$column}'),";
            if ($key < count($columns) - 1) {
                $stub .= PHP_EOL . str_repeat(' ', $indentation);
            }
        }
        return $stub;
    }

    protected function dataTableEdit($definition, $indentation = 12) {
        $columns = is_array($definition) ? $definition : explode(',', $definition);
        $stub = '';
        foreach ($columns as $key => $column) {
            $stub .= "'{$column}' => \$query->{$column},";
            if ($key < count($columns) - 1) {
                $stub .= PHP_EOL . str_repeat(' ', $indentation);
            }
        }
        return $stub;
    }

    protected function buildReplacements($tableName, $type) {
        $arrayReplacements = $this->arrayReplacements($tableName, $type);
        $replace = [];
        foreach ($arrayReplacements as $key => $arrayReplacement) {
            $replace['{{ ' . $key . ' }}'] = $arrayReplacement;
            $replace['{{' . $key . '}}'] = $arrayReplacement;
        }
        return $replace;
    }

}
