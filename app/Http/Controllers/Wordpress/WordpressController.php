<?php

namespace App\Http\Controllers\Wordpress;

use App\Http\Controllers\Controller;

class WordpressController extends Controller {

    public function index($any = null) {
        $baseUrl = basename(request()->url());
        if ($baseUrl && file_exists($baseUrl)) {
            require_once base_path("public/{$baseUrl}");
            return;
        } else {
            require base_path("public/wp-index.php");
            exit;
        }
    }

}
