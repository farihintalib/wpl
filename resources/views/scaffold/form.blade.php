<div class="form-group">
    <label for="generate_path">Generate Path</label>
    <input type="text" class="form-control" id="generate_path" name="generate_path" value="{{$generatePath}}">
</div>
<div class="form-group">
    <label for="theme">Theme</label>
    {!! Form::select('theme',$option()->select('themes'),request()->theme,['class'=>'custom-select']) !!}
</div>
<div class="form-group">
    <label for="name">Name</label>
    {!! Form::select('name',$option()->select('scaffoldName'),request()->name,['class'=>'custom-select']) !!}
</div>
<div class="form-group">
    <label for="module">Module/Submodule</label>
    <input type="text" class="form-control" id="module" name="module" value="{{$module}}">
</div>
<div class="form-group">
    <label for="database">Database</label>
    <div class="input-group">
        {!! Form::select('database',$option()->select('database'),$database,['class'=>'custom-select']) !!}
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit">Refresh</button>
        </div>
    </div>
</div>
<div class='form-group'>
    <label for="table">Table</label>
    <div class="input-group">
        <select name="table" class="custom-select" id="inputGroupSelect04" aria-label="Example select with button addon">
            <option value="">Choose...</option>
            @foreach($tables as $table)
                <option value="{{$table}}" {{ $selected($table,request()->table) }}>{{$table}}</option>
            @endforeach
        </select>
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit">Preview</button>
        </div>
    </div>
</div>
