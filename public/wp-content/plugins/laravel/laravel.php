<?php

/*
  Plugin Name: Laravel
  Description: Laravel
  Version: 1.0.0
  Author: iNKiLLeRz
 */

define('LARAVEL_PATH', __dir__ . '/../../../');
define('WPL__PLUGIN_DIR', plugin_dir_path(__FILE__));
require_once( WPL__PLUGIN_DIR . 'class.laravel.php' );

add_action('wp_login', array('Laravel', 'login'));
add_action('wp_logout', array('Laravel', 'logout'));
add_action('template_redirect', array('Laravel', 'redirect404'));
